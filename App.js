/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { View } from 'react-native';
import FlipCountdown from './components/flipCountdown/index';
import styles from './Style';

const style = {
  color: '#f09',
  backgroundColor: '#0f0',
};

export default function App() {
  return (
    <View style={styles.container}>
      <FlipCountdown
        endDate={new Date('Feb 1, 2019 10:35:25')}
        bottomStyle={style}
        perspective={50}
        bgImage={{ uri: 'https://dummyimage.com/100x100/f90/000.png&text=...' }}
      />
    </View>
  );
}
