// @flow
import React from 'react';
import FlipChar from './FlipChar/FlipChar';
import type { CharsT, DatePartsT } from './types';

function splitDigits(num: number): CharsT {
  return (num < 10 ? `0${num}` : num).toString().split('');
}

export function toParts(endDate: number): DatePartsT {
  const currDate = Date.now();
  const distance = endDate - currDate;

  const days = Math.floor(distance / (1000 * 60 * 60 * 24));
  const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((distance % (1000 * 60)) / 1000);

  return {
    days: splitDigits(days),
    hours: splitDigits(hours),
    minutes: splitDigits(minutes),
    seconds: splitDigits(seconds),
  };
}

export function buildFlips(chars: CharsT, props: Object, animated: *): Array<React$Node> {
  // eslint-disable-next-line react/no-array-index-key
  return chars.map((char, idx) => (
    <FlipChar {...props} key={idx} flipChar={char} animated={animated} />
  ));
}
