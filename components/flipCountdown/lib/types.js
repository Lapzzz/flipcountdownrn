// @flow

import type {
  TextStyleProp,
  ViewStyleProp,
} from 'react-native/Libraries/StyleSheet/StyleSheetTypes';

export type CharsT = Array<string>;

export type DatePartsT = {|
  days: CharsT,
  hours: CharsT,
  minutes: CharsT,
  seconds: CharsT,
|};

export type LabelsT = {|
  days: string,
  hours: string,
  minutes: string,
  seconds: string,
|};

export type BasePropsT = {
  endDate: Date,
  perspective?: number,
  useNativeDriver?: boolean,
  friction?: number,
  topStyle?: ViewStyleProp,
  bottomStyle?: ViewStyleProp,
  bgImage?: string,
  showLabel?: boolean,
  labelsStyle?: TextStyleProp,
  labels?: LabelsT,
};
