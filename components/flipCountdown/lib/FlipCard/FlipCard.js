// @flow
import React from 'react';
import { View, Animated, StyleSheet } from 'react-native';
import type { BasePropsT } from '../types';

type PropsT = {
  ...BasePropsT,
  animated: *,
  children: *,
};

const styles = StyleSheet.create({
  card: {
    transform: [{ scaleY: -1 }, { translateY: 0 }],
  },
});

export default function FlipCard(props: PropsT) {
  const { perspective, animated, children, ...other } = props;
  const transform = perspective ? [{ perspective }] : [];

  transform.push(
    {
      rotateX: animated.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '-180deg'],
      }),
    },
    {
      translateY: animated.interpolate({
        inputRange: [0, 1],
        outputRange: [0, -20],
      }),
    },
  );

  return (
    <Animated.View {...other} style={{ transform, zIndex: 10 }}>
      <View style={[styles.card, {}]}>{children}</View>
    </Animated.View>
  );
}
