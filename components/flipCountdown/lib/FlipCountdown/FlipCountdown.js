// @flow
import React from 'react';
import { View, Text } from 'react-native';

import type { DatePartsT, BasePropsT } from '../types';

import BaseFlipChars from '../BaseFlipChars/BaseFlipChars';
import { buildFlips, toParts } from '../utils';

import styles from './styles';

type StateT = {
  animation: *,
  endDate: ?number,
  timeLeftPairs: DatePartsT,
};

export default class FlipCountdown extends BaseFlipChars<BasePropsT, StateT> {
  static defaultProps = {
    labels: {
      days: 'Days',
      hours: 'Hours',
      minutes: 'Minutes',
      seconds: 'Seconds',
    },
    showLabel: true,
  };
  constructor(props: BasePropsT) {
    super(props);
    const endDate = this.props.endDate.getTime();

    this.state = {
      ...this.state,
      endDate,
      timeLeftPairs: toParts(endDate),
    };
  }

  onInterval() {
    if (this.state.endDate <= new Date().getTime()) {
      clearInterval(this.interval);
      return;
    }

    const timeLeftPairs = toParts(this.state.endDate);
    this.setState({ timeLeftPairs });
  }

  render() {
    const animated = this.state
      .animation; /*.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '-180deg'],
    });*/
    const { labelsStyle, labels, showLabel, ...flipCharProps } = this.props;

    return (
      <View style={styles.row}>
        <View>
          <View style={styles.row}>
            {buildFlips(this.state.timeLeftPairs.days, flipCharProps, animated)}
            <Text style={styles.dots}>:</Text>
          </View>
          {showLabel ? (
            /* $FlowFixMe  Default Props not understood by flow */
            <Text style={[styles.label, labelsStyle]}>{labels.days}</Text>
          ) : null}
        </View>
        <View>
          <View style={styles.row}>
            {buildFlips(this.state.timeLeftPairs.hours, flipCharProps, animated)}
            <Text style={styles.dots}>:</Text>
          </View>
          {showLabel ? (
            /* $FlowFixMe  Default Props not understood by flow */
            <Text style={[styles.label, labelsStyle]}>{labels.hours}</Text>
          ) : null}
        </View>
        <View>
          <View style={styles.row}>
            {buildFlips(this.state.timeLeftPairs.minutes, flipCharProps, animated)}
            <Text style={styles.dots}>:</Text>
          </View>
          {showLabel ? (
            /* $FlowFixMe  Default Props not understood by flow */
            <Text style={[styles.label, labelsStyle]}>{labels.minutes}</Text>
          ) : null}
        </View>
        <View>
          <View style={styles.row}>
            {buildFlips(this.state.timeLeftPairs.seconds, flipCharProps, animated)}
          </View>
          {showLabel ? (
            /* $FlowFixMe  Default Props not understood by flow */
            <Text style={[styles.label, labelsStyle]}>{labels.seconds}</Text>
          ) : null}
        </View>
      </View>
    );
  }
}
