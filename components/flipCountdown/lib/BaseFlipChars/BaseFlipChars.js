// @flow
import { Component } from 'react';
import { Animated } from 'react-native';

export default class BaseFlipChars<P: {}, S: any> extends Component<P, S> {
  // eslint-disable-next-line react/sort-comp
  interval: IntervalID;

  state = {
    animation: new Animated.Value(0),
  };
  period = 1000;

  // eslint-disable-next-line class-methods-use-this
  onInterval() {}

  componentDidMount() {
    this.interval = setInterval(() => {
      this.onInterval();

      this.state.animation.setValue(0);
      Animated.spring(this.state.animation, {
        toValue: 1,
        friction: 6,
        useNativeDriver: true,
      }).start();
    }, this.period);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }
}
