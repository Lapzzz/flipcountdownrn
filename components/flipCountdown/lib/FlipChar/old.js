// @flow

import FlipCard from '../flipCard/FlipCard';
import React from 'react';
import { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

const WIDTH = 40;

export default class FlipChar extends Component {
  state = {
    current: this.props.flipChar,
    prev: this.props.flipChar,
  };

  componentWillReceiveProps(nextProps) {
    this.setState(state => ({
      current: nextProps.flipChar,
      prev: state.current,
    }));
  }

  renderHalf(styles, text) {
    if (this.props.bg) {
      return (
        <Image style={styles} source={this.props.bg}>
          <Text style={flipCharStyles.txt}>{text}</Text>
        </Image>
      );
    }

    return (
      <View style={styles}>
        <Text style={flipCharStyles.txt}>{text}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={flipCharStyles.card}>
        {this.renderHalf([flipCharStyles.halfTop, flipCharStyles.absTop], this.state.current)}
        {this.renderHalf([flipCharStyles.halfBottom, flipCharStyles.absBottom], this.state.prev)}
        {this.state.current !== this.state.prev ? (
          <FlipCard style={{ width: WIDTH, zIndex: 10 }} animation={this.props.animation}>
            <View style={flipCharStyles.halfBottom}>
              <Text style={[flipCharStyles.txt, { backgroundColor: '#b3b3cc' }]}>
                {this.state.current}
              </Text>
            </View>
          </FlipCard>
        ) : null}
      </View>
    );
  }
}

const flipCharStyles = StyleSheet.create({
  txt: {
    color: '#000',
    fontSize: WIDTH,
    fontWeight: 'bold',
    backgroundColor: '#b3b3cc',
    lineHeight: WIDTH,
    height: WIDTH,
    paddingTop: 3, //fixed value
    paddingLeft: 2, //fixed value
    textAlign: 'center',
  },
  absTop: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  halfTop: {
    height: WIDTH / 2,
    overflow: 'hidden',
    width: WIDTH,
    borderRadius: 3,
  },
  absBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  halfBottom: {
    height: WIDTH / 2,
    overflow: 'hidden',
    justifyContent: 'flex-end',
    width: WIDTH,
    borderRadius: 3,
  },
  card: {
    height: WIDTH,
    width: WIDTH,
    alignSelf: 'flex-end',
  },
});
