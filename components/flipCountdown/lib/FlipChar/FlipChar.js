// @flow

import React, { Component } from 'react';
import { View, Text, ImageBackground, Animated } from 'react-native';

import type {
  ViewStyleProp,
  ImageStyleProp,
  TextStyleProp,
} from 'react-native/Libraries/StyleSheet/StyleSheetTypes';
import type { BasePropsT } from '../types';

import FlipCard from '../FlipCard/FlipCard';
import styles from './styles';

type PropsT = {
  ...BasePropsT,
  flipChar: string,
  animated: *,
};
type StateT = {
  current: string,
  prev: string,
};

export default class FlipChar extends Component<PropsT, StateT> {
  static defaultProps = {
    topStyle: null,
    bottomStyle: null,
    animated: new Animated.Value(0),
    flipChar: null,
  };

  state = {
    current: this.props.flipChar,
    prev: this.props.flipChar,
  };

  componentWillReceiveProps(nextProps: PropsT) {
    this.setState(state => ({
      current: nextProps.flipChar,
      prev: state.current,
    }));
  }

  renderHalf(style: ViewStyleProp | ImageStyleProp, txtStyle: TextStyleProp, text: string) {
    if (this.props.bgImage) {
      return (
        <ImageBackground style={[style, styles.image]} source={this.props.bgImage}>
          <Text style={[styles.layoutTxt, txtStyle]}>{text}</Text>
        </ImageBackground>
      );
    }

    return (
      <View style={style}>
        <Text style={[styles.layoutTxt, txtStyle]}>{text}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.card}>
        {this.renderHalf(
          [styles.halfTop, styles.absTop],
          [styles.txt /*this.props.topStyle*/],
          this.state.current,
        )}
        {this.renderHalf(
          [styles.halfBottom, styles.absBottom],
          [styles.txt /*, this.props.bottomStyle*/],
          this.state.prev,
        )}
        {this.state.current !== this.state.prev ? (
          <FlipCard animated={this.props.animated} perspective={this.props.perspective}>
            {this.renderHalf(
              [styles.halfBottom],
              [styles.txt /*, this.props.bottomStyle*/],
              this.state.current,
            )}
          </FlipCard>
        ) : null}
      </View>
    );
  }
}
