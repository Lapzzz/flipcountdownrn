// @flow

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  layoutTxt: {
    fontSize: 40,
    fontWeight: 'bold',
    lineHeight: 40,
    height: 40,
    paddingTop: 3,
    paddingLeft: 2,
    textAlign: 'center',
  },
  absTop: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  halfTop: {
    height: 20,
    overflow: 'hidden',
    width: 40,
    borderRadius: 3,
  },
  txt: {
    //    backgroundColor: '#b3b3cc',
    color: '#000',
  },
  absBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  halfBottom: {
    height: 20,
    overflow: 'hidden',
    justifyContent: 'flex-end',
    width: 40,
    borderRadius: 3,
  },
  card: {
    height: 40,
    width: 40,
    alignSelf: 'flex-end',
  },
  image: {
    height: 20,
    width: 40,
  },
});
